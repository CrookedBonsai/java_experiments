public class discount extends movieClass {

	// Constructor Class
	public discount(double defaultTicketPrice) 
	{
		super(defaultTicketPrice);
	}

	public double calculateDiscount(double cost)
	{
		if (cost > 100)
		{
			cost = (cost - (cost / 5));
		}
		return cost;
	}
}