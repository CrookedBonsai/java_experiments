package A1;

import java.util.*;

public class Testing 
{

	int[] ia = new int[1];
	Object oA[]  = new Object[1];
	boolean bool;
	   
	public static void main(String[] args) 
	{

		// Check triple level arrays
		int[] multiArray[] = new int[2][3];
		int[] multiArray2[] = {{1,2,3},{4,5,6}};
		int[][] multiArray3 = {{1,2,3},{4,5,6}};
		int[][][] multiArray4 = { {{1,1,1},{2,2,2}}, {{3,3,3},{4,4,4}} };
		
		// Enhanced Loop
		for(int[] item: multiArray3)
		{
			for(int item2: item)
			{
				System.out.print(item2 + ", ");
			}
			System.out.println();
		}
		
//		multiArray.length[1][1];
		
		System.out.println("\n3 Level Array:");
		for(int[][] item: multiArray4)
		{
			for(int[] item1: item)
			{
				for(int item2: item1)
				{
					System.out.print(item2 + ", ");
				}
				System.out.println();
			}
			System.out.println();
		}
		
		
		// create array of 0 length yes but u cant use it
		System.out.println("\nArray of 0 length:");
		String[] zeroArray = new String[0];
		String[] emptyArray = {null};
		String output = "";
		
		output = (zeroArray == emptyArray) ? "True" : "False";

		System.out.println(output);
//		zeroArray[0] = "test";
//		System.out.println("Zero Array = " + zeroArray[0]);
		
		
		
		// try to create object[]
		cat catArray[] = new cat[1];
		System.out.println("Cat array = " + catArray[0]);
		
		
	   
		System.out.println("Object array = ");
		Testing test = new Testing();
		System.out.println(test.ia[0] + "  " + test.oA[0]+"  "+test.bool);

	
		
		// ArrayList? which library?
		

	
		// Sandbox
		System.out.println("\nSandbox");
		  int i = 0 ;
		  int[] iA = {10, 20} ;
		  iA[i] = i = 30 ;
		  System.out.println(""+ iA[ 0 ] + " " + iA[ 1 ] + "  "+i);
//		  
//		  iA.length();
//		  
//		  iA.size();
		  
		  
		  
		  boolean[] boo = new boolean[1];
		  System.out.println("Boo = " + boo[0]);
		  
		  ArrayList aList = new ArrayList();
		  String[] collection = {"D", "E", "F"};
		  
		  aList.add("A");
		  aList.add("B");
		  aList.add("C");
		  aList.add(1,"D");
		  aList.add(1);
		  
		  System.out.println("List length = " + aList.size());
		  
//		  aList.addAll(collection);
		  
		  System.out.println("Collection = " + aList);
		  
		  aList.remove(1);
		  aList.remove("C");
		  System.out.println("Collection = " + aList);
		  


		  String[] array10 = {"a", "b", "c"};
		  int[] array11 = {1,2,3};
		  System.out.println("Length = " + array10.length);
		  System.out.println("Length = " + array11.length);
		
		  
		  int[] a2 = { 1, 2, 3, 4 };
	      int[] b2 = { 2, 3, 1, 0 };
	      System.out.println( a2 [ (a2 = b2)[3] ] );

		  
	      int size = 10;
	      int[] arr = new int[size];
	      for (int ip = 0 ; ip < size ; ++ip) System.out.println(arr[ip]);

	     int[] iP;
	     
	      iP = new int[]{1,2,3};
	      
	      System.out.println("IP = " + iP);
	      
	      char cA[][] = new char[3][];
	      
	      
	 
	      int index = 1;
	      String[] strArr = new String[5];
	      String   myStr  = strArr[index];
	      System.out.println(myStr);

	      double daaa[][][] = new double[3][][];
	      double d = 100.0;
	      double[][] daa = new double[1][1];
	      
	      daaa[0] = d; 
	      daaa[0] = daa; 
	      daaa[0] = daa[0]; 
	      daa[1][1] = d; 
	      daa = daaa[0];




	}
	
	public void test()
	{
		try {
	      saveObject( new ArrayList() ); 
	      Collection c = new ArrayList(); saveObject( c ); 
	      List l = new ArrayList(); saveObject(l); 
	      saveObject(null); 
	      saveObject(0); //The argument is the number zero and not the letter o
		} catch(Exception e) {
			System.out.println("Exception");
		}
		
	}
	
    int i6 = 0 ;
    Object prevObject ;
    public void saveObject(List e ){
        prevObject = e ;
        i6++ ;
    }

 

}

