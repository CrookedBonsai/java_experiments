package A1;

import java.io.File;
import java.io.PrintWriter;
import java.util.*;


public class MovieGames 
{
	private static Scanner inputFile;
//	private static PrintWriter outputFile;
	
//	private static String[] filmNames  	= new String[8];
//	private static String[] actorNames 	= new String[8];
	private static String[][] filmData 	= new String[8][3];
	
	Scanner myStringScanner = new Scanner(System.in);
	Scanner myIntScanner 	= new Scanner(System.in);
	
	Scanner myInputFile = new Scanner("C:/Users/Andy/Documents/My Documents/Training/Java/LoyaltyFile.txt");
	
	/**
	 * Movie Game
	 * If the actor of a film is correctly guessed, then the tickets are free
	 */	
	public String[][] setupMovieGame()
	{
		String dataRow = "";
		int i = 0;
		try
		{
			// Setting up Read File
			File filmData = new File ("C:/development/dev/JavaWorkspace/Fundamentals/src/A1/MoviesFile.txt");
			inputFile = new Scanner(filmData);
			inputFile.useDelimiter(";");

		} catch(Exception e) {
			System.out.println("Error: " + e);
		}
		
		// Reading the data from the file into an array
		while (inputFile.hasNext())
		{
			try
			{
				dataRow = inputFile.next();
				
				// Write the movies into an array from the MoviesFile
				String[] fileData = dataRow.split(",");
//				System.out.println("Row = " + dataRow);
				
				// saving the Film Name and the Actor Name in separate arrays
				filmData[i][0] = fileData[2];
				filmData[i][1] = fileData[3];
				filmData[i][2] = fileData[4];
				
//				System.out.println("Film Name = " 	+ fileData[2]);
//				System.out.println("Main Actor = " 	+ fileData[3]);
//				System.out.println("Actors = " 		+ fileData[4]);
				
				i++;
			} catch(Exception e) {
//				System.out.println("EndOfFile");
			}
		}
		
		inputFile.close();		
		
		return filmData;
	}
	
	// Return a character indicatin which game (or none) will be played
	public char whichGameToPlay()
	{
		int answer = 1;
		char result = 'n';
//		Scanner myStringScanner = new Scanner(System.in);
//		Scanner myIntScanner 	= new Scanner(System.in);
		
		System.out.println("Would you like to have a chance at winning free tickets? (y or n)");
		result = myStringScanner.nextLine().toLowerCase().charAt(0);
	
		if (result == 'y')
		{
			do {
				System.out.println("\n----------------\nWelcome to the Games Station\nWhich Game would you like to play? (choose a number:)\n1 - None\n2 - Movie Game\n3 - Hangman");
				answer = myIntScanner.nextInt();
				
				result = 'x';
				
				switch(answer)
				{
					case 1:
						result = 'n';
						System.out.println("You have chosen game " + answer + " - None - Skipping Game");
						break;
					case 2:
						result = 'm';
						System.out.println("You have chosen game " + answer + " - Movie Game");
						break;
					case 3:
						result = 'h';
						System.out.println("You have chosen game " + answer + " - Hangman");
						break;
					default:
						result = 'n';
						System.out.println("You have chosen game " + answer + " - None - Skipping Game");
						break;
				}
			} while (result == 'x');
		} else {
			System.out.println("You have opted to skip playing a game for free tickets. Continuing to payment");
			result = 'n';
		}
//		myStringScanner.close();
//		myIntScanner.close();
		return result;
	}
	
	
	// ask the user for a number from 1 to 7
	public int getUserChoice()
	{
		int userChoice 		 = 0;
		int input	 		 = 0;
//		Scanner myIntScanner = new Scanner(System.in);
		
		System.out.println("\n\n----------------------------\nWelcome to the Movie Game!!\n----------------------------");
		
		do
		{
			System.out.println("Please pick a number from 1 to 7 (Inclusive)...");
			input = myIntScanner.nextInt();
			if ((input <= 7) && (input >=1))
			{
				userChoice = input;
				System.out.println("Thank you. You have chosen: " + userChoice);
			} else {
				System.out.println("Your input was not correct. Please try again.");
			}
		} while (userChoice == 0);
		
//		myIntScanner.close();
		return userChoice;
	}
	
	
	// Ask the user for an actor name for that film
	public String getUserGuess(String[][] filmData, int userChoice)
	{
		String userGuess = ""; 
//		Scanner myStringScanner = new Scanner(System.in);
		
		System.out.println("\n-----------------------\nFor the Chance to win free tickets:\nQ. Name an actor who starred in the film: ");
		System.out.println(filmData[userChoice][0]);
		System.out.println("...");
		
		userGuess = myStringScanner.nextLine();
//		userGuess = "Al Pacino";
		System.out.println("Thank you for your guess of: " + userGuess);
		
//		myStringScanner.close();
		return userGuess;
	}
	
	
	// Check the guess against the actor in the array and return true or false
	public boolean checkGuess(String[][] filmData, int userChoice, String userGuess)
	{
		boolean result 			= false;
		String filmName			= filmData[userChoice][0];
		String mainActor 		= filmData[userChoice][1];
		String actors			= filmData[userChoice][2];
		String userGuessTrimmed = userGuess.toLowerCase().trim();
		String mainActorTrimmed = mainActor.toLowerCase().trim();
		
		if (mainActorTrimmed.equals(userGuessTrimmed))
		{
			System.out.println("Congratulations!!! you correctly guessed that: " + userGuess);
			System.out.println("is in the film: " + filmName);
			System.out.println("You have won free tickets!!!!\nEnjoy the film :-)");
			result = true;
		} else if (actors.toLowerCase().contains(userGuessTrimmed)) {
			System.out.println("Congratulations!!! you correctly guessed that: " + userGuess);
			System.out.println("is a supporting actor in the film: " + filmName);
			System.out.println("You have won free tickets!!!!\nEnjoy the film :-)");
			result = true;
		} else {
			System.out.println("You fail!!! the actor(ess): " + userGuess);
			System.out.println("is defiitely NOT in the film: " + filmName);
			result = false;
		}
		System.out.println("The Leading Actor was: " + mainActor);
		System.out.println("------------------------------\n");
		return result;
	}
	
	public void end()
	{
		myStringScanner.close();
		myIntScanner.close();
	}
	
}
