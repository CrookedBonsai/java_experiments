package A1;

interface MovieLoyaltyInterface 
{
	abstract void setName();
	
	abstract boolean checkName(String name, String newName);
	
	abstract double applyLoyaltyDiscount(double cost, int numberOfTickets);
}
