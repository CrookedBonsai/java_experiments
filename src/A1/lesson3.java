package A1;

import java.util.Scanner;

public class lesson3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		int num = 7;
//		
//		for(int x=1; x<=5; x++) {
//			System.out.println("Number = " + num);
//			System.out.println("This number has been printed " + x + " times.");
//		}
		
		/**
		 *  Ask user to guess the random number
		 *  They get three tries
		 *  Then ask if they want to play again
		 */
		char playAgainAnswer 	= 'y';
		boolean playAgain		= false;
		int number 				= 0;
		int answer				= 0;
		
		Scanner myStringScanner = new Scanner(System.in);
		Scanner myIntScanner 	= new Scanner(System.in);
		
		do{
			// Set up the random number and game instructions
			number = (int)(Math.random() * 10);
			System.out.println("The random number is: " + number);
			System.out.println("Welome to the game\nTry to guess the random number. You have three tries:\n(hint: it is between 1 and 10)");
			
			// Allow three guesses and check each against the answer
			for (int x=1; x<=3; x++) {
				System.out.println("Attempt number " + x + " of 3:");
				answer = myIntScanner.nextInt();
				if (answer == number) {
					System.out.println("Congratulations! You Win!!!\nYou correctly guessed the secret number: " + number);
					break;
				} else {
					System.out.println("Incorrect: your guess of " + answer + " is not correct.");
					if (x != 3) {
						System.out.println("Please try again.");
					}
				}
			}
			
			// Check if they would like to play again
			System.out.println("Would you like to play again? (y or n)");
			playAgainAnswer = myStringScanner.next().charAt(0);
			playAgain = (playAgainAnswer == 'y' || playAgainAnswer == 'Y') ? true : false;
		} while (playAgain == true);
	}

}
