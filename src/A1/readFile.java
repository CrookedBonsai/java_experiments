import java.util.*;
import java.io.*;

public class readFile
{
	public static Scanner inputFile;
	public static PrintWriter outputFile;

	public static void main (String[] args)
	{
		String stuff 	= "";
		int counter 	= 0;

		try 
		{
			File students = new File ("C:/development/dev/JavaWorkspace/Fundamentals/src/A1/testCsvFile.csv");
			inputFile = new Scanner(students);
			inputFile.useDelimiter(";");
			outputFile = new PrintWriter(new File("testOut.txt"));
			System.out.println("An input file opened and created a blank output file");
		} catch(Exception e) {
			System.out.println("Error: " + e);
		}

		while (inputFile.hasNext())
		{
			stuff = inputFile.next();
			outputFile.print(stuff + ";");

			// Split the files row into an array by delimiter of a comma
			String[] rowArray = stuff.split(",");

			System.out.println("Their First Name is: " + rowArray[1] + " and their Age is: " + rowArray[3]);

			counter++;
			System.out.println(stuff);
		}
		System.out.println(counter + " is the number of records in the file");

		inputFile.close();
		outputFile.close();
	}
}