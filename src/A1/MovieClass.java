package A1;

import java.text.DecimalFormat;
import java.util.Scanner;

public class MovieClass
{	
	public double defaultTicketPrice	= 0.00;

	// Constructor Class
	public MovieClass(/**double defaultTicketPrice**/)
	{
		// EXAMPLE for instantiating within the constructor
		// NOTE: the use of "this." to refer to the class variable above. 
		// Otherwise anything declared here will only be visible in the constructor and not for the rest of the class
		
		// this.defaultTicketPrice = defaultTicketPrice;
		// System.out.println("The default ticket price has been set to: " + this.defaultTicketPrice);
	}	

	// EXAMPLE for declaring an abstract method, which MUST then be inherited and Overridden
	// abstract double calculateDiscount(double cost);

	public int numberOfPeople 			= 0;
	public char ageGroup 				= ' ';
	String ageGroupVerbose 				= "";
	public double fee 					= 00.00;
	double cost							= 00.00;
	double change 						= 0;

	
	Scanner myStringScanner = new Scanner(System.in);
	Scanner myIntScanner 	= new Scanner(System.in);
	DecimalFormat df 		= new DecimalFormat("#0.00");


	// Get input data
	public void getInput() {
		System.out.println("Welcome\nHow many people are going to the Cinema?");
		numberOfPeople = myIntScanner.nextInt();
		System.out.println("There are " + numberOfPeople + " attending the movies");
		
		System.out.println("\n--------------\nWhich age group are the film-goers? (Please chose one from below):");
		System.out.println("Adult\t\t=\t'A'\t�" + df.format(defaultTicketPrice) + "\nChild\t\t=\t'C'\t� 7:50\nPensioner\t=\t'P'\t� 3:00");
		ageGroup = myStringScanner.next().charAt(0);
		System.out.println("\n\n--------------");
	}
	
	// Work out the age group
	public String getAgeGroup(char ageGroupInput){
		
		switch (ageGroupInput){
			case 'a':
			case 'A': fee = defaultTicketPrice;
					  ageGroupVerbose = "Adult";
					  break;
			case 'c':
			case 'C': fee = 7.50;
					  ageGroupVerbose = "Child";
					  break;
			case 'p':
			case 'P': fee = 3.00;
					  ageGroupVerbose = "Pensioner";
					  break;
			default:  fee = defaultTicketPrice;
					  ageGroupVerbose = "Default";
		}
		
		return ageGroupVerbose;
	}
	
	
	// Work out the cost
	public double calculateCost() {
		cost = numberOfPeople * fee;
		System.out.println("The cost for the tickets = no of people: " + numberOfPeople + " * fee: " + fee + " = �" + df.format(cost));
		return cost;
	}
	
	// Calculate the change
	public double calculateChange(double payment, double cost) {
		if (cost <= payment) {
			change = payment - cost;
			System.out.println("Thank you for your payment of �" + df.format(payment) + ", your change is: �" + df.format(change) + "\n--------------------");
		} else {
			change = cost - payment;
			System.out.println("You have insufficient funds, please pay an additional: �" + df.format(change));
		}
		
		return change;
	}

	public void setDefaultPrice(double defaultPrice)
	{
		this.defaultTicketPrice = defaultPrice;
		System.out.println("The default ticket price is been set by the setter to: �" + df.format(this.defaultTicketPrice));
	}

	public double getDefaultPrice()
	{
		System.out.println("Returning the default ticket price via the getter of: " + df.format(this.defaultTicketPrice));
		return this.defaultTicketPrice;
	}

}
