package A1;

public class lesson4 {

	public static void main(String[] args) {
		// String Class
		
		String string1 = "Hello";
		String string2 = "World";
		String string3 = ("Welcome to PCWorkshopslondon.co.uk");
		
		// Concat
		System.out.println("Concatenate");
		String string4 = string1.concat(" ").concat(string2);
		System.out.println(string4);
		
		// Contains
		System.out.println("\nContains:");
		if (string3.contains("yo")) System.out.print("You cannot spell");
		if (string3.contains("london")) System.out.print("This is spelled correctly");
		
		// Length
		System.out.println("\n\nLength");
		int len = string3.length();
		System.out.println("Length = " + len);
		
		// To Uppercase
		System.out.println("\n\nUppercase");
		System.out.println(string3.toUpperCase());
		
		// To Lowercase
		System.out.println("\n\nLowercase");
		System.out.println(string3.toLowerCase());
		
		// Substring
		System.out.println("\n\nSubstring - 0 to 4");
		System.out.println(string3.substring(10, 15));
		
		
		// index of
		System.out.println("\n\nIndex Of");
		String myStringVariable = ("Welcome to PCWorkshopslondon.co.uk");
		System.out.println(myStringVariable.indexOf("london",2));
		

	}

}
