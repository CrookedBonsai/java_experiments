public class catController 
{
	public static animal myAnimal;

	public static void main(String[] args)
	{
		int choice = 0;

		System.out.println("Please choose an animal\n1 - Animal\n2 - Cat\n3 - Wild Cat\n4 - Lion");
		if (args.length == 0)
		{
			System.out.println("No animal selection detected. Please try again");
		} else {
			choice = Integer.parseInt(args[0]);
			System.out.println("You have chosen option: " + choice);

			switch (choice)
			{
				case 1: 
					myAnimal = new animal();
					break;
				case 2: 
					myAnimal = new cat();
					break;
				case 3: 
					myAnimal = new wildCat();
					break;
				case 4: 
					myAnimal = new lion();
					break;
			}

			myAnimal.move();
			myAnimal.saySomething();
		}
	}
}