import java.util.*;
import java.io.*;

public class readFileStream
{
	public static void main (String[] args)
	{
		int i 			= 0;
		double pricesIN = 0.0;
		int balanceIN	= 0;
		String nameIN	= "";

		double[] prices = {19.99, 9.99, 15.99, 3.99, 4.99};
		int[] units 	= {12, 8, 13, 29, 50};
		String[] descs	= {"Joe Bloggs", "John Smith", "Dina Manison Martins", "Pete Drummer", "Kelvin Luther Klein"};


		// WRITE the file
		try 
		{
			FileOutputStream balancesFiles 	= new FileOutputStream("C:/development/dev/JavaWorkspace/Fundamentals/src/A1/balances2.dat");
			ObjectOutputStream outStream 	= new ObjectOutputStream(balancesFiles);

			for(i=0; i<prices.length; i++)
			{
				outStream.writeDouble(prices[i]);
				outStream.writeInt(units[i]);
				outStream.writeUTF(descs[i]);
			}

			outStream.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("IO Exception");
		}


		// READ the File
		try {
			FileInputStream balancesFile 	= new FileInputStream("C:/development/dev/JavaWorkspace/Fundamentals/src/A1/balances2.dat");
			ObjectInputStream inStream 		= new ObjectInputStream(balancesFile);

			while (inStream != null)
			{
				pricesIN 	= inStream.readDouble();
				balanceIN 	= inStream.readInt();
				nameIN 		= inStream.readUTF();
				System.out.println("PricesIN: " + pricesIN + "\t - BalanceIN: " + balanceIN + "  \t - NameIN: " + nameIN);
			}
			inStream.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("IO Error");
			// Might be a null because it has read one line past the end of the file
			// either handle checking if the inStream == null here,
			// or add a counter on the Write loop and loop until that number is hit || null

		}
	}
}