package A1;

import java.io.*;
import java.util.*;

public class MovieLoyalty implements MovieLoyaltyInterface 
{
	private String  inputName = "";
	private Scanner inputFile;
	private PrintWriter outputFile;
	
	Scanner myStringScanner = new Scanner(System.in);
	
	public void setName()
	{
		System.out.println("Please enter your name...");
		inputName = myStringScanner.nextLine();
		System.out.println("Thank you " + inputName);
	}
	
	
	public boolean checkName(String fileName, String newName)
	{
		fileName 	= fileName.toLowerCase().trim();
		newName 	= newName.toLowerCase().trim();
		
		if (fileName.equals(newName))
		{
//			System.out.println("Match found!");
			return true;
		} else {
//			System.out.println("No!");
			return false;
		}
	}
	
	public double calculateDiscount(String loyaltyPointsString, double cost)
	{
		int loyaltyPoints = 0;
		loyaltyPoints = Integer.parseInt(loyaltyPointsString);
		
		cost = cost - (Math.floor(loyaltyPoints / 2));;
		System.out.println("Congratulations - your " + loyaltyPoints + " points have discounted your ticket to: �" + cost);
		return cost;
	}
	
	public String addLoyaltyPoints(String[] row, int noOfTickets)
	{
		String result = "";
		int points = Integer.parseInt(row[1]) + noOfTickets;
		result = row[0].concat(", ").concat(Integer.toString(points));

		System.out.println("Adding Loyalty points: output = " + result);
		
		return result;
	}
	
	
	public double applyLoyaltyDiscount(double cost, int numberOfTickets)
	{
		String fileRow 	= "";
		boolean found 	= false;
		boolean existingCustomer = false;
		
		try
		{
			File loyaltyFile = new File("C:/development/dev/JavaWorkspace/Fundamentals/src/A1/LoyaltyFile.txt");
			inputFile = new Scanner(loyaltyFile);
			inputFile.useDelimiter(";");
			outputFile = new PrintWriter(new File("C:/development/dev/JavaWorkspace/Fundamentals/src/A1/LoyaltyFileOutput.txt"));
		} catch(Exception e) {
			System.out.println("Exception hit: " + e);
		}
		
		char participate = 'n';
		System.out.println("\n------------------\nWould you like to participate in the loyalty sceme? (y or n)");
		participate = myStringScanner.nextLine().toLowerCase().charAt(0);
		
		if (participate == 'y')
		{
			setName();
			
			// read file
			while(inputFile.hasNext())
			{
				fileRow = inputFile.next();
				String[] fileRowArray = fileRow.split(", ");
				
				found = checkName(fileRowArray[0], inputName);
			
				if (found == true)
				{
					System.out.println("\n---------------\nWe have you on record: Calculating Loyalty Discount");
					cost = calculateDiscount(fileRowArray[1], cost);
					System.out.println("New Cost is Now: �" + cost + "\n--------------------\n");
					fileRow = addLoyaltyPoints(fileRowArray, numberOfTickets);
					existingCustomer = true;
					found = false;
				}
				
				// write to output file
				System.out.println("Writing row: " + fileRow.trim() + ";");
				outputFile.print(fileRow.trim() + ";\n");
				
				if (inputFile.hasNext() == false && existingCustomer == false)
				{
					System.out.println("You are a new customer " + inputName + ", We have added: " + numberOfTickets + " points to your loyalty account.");
					outputFile.println(inputName.concat(", ") + numberOfTickets + ";\n");
				}
				
			}
		} else {
			System.out.println("You are not participating in the loyalty scheme, so there is no change to the cost");
		}
		outputFile.close();
		
		return cost;
	}
}
