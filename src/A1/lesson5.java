package A1;

public class lesson5 {
	
	public static void main(String[] args) {
		// Arrays
		
		// Simple Array
		System.out.println("Simple Array");
		int[] x;
		x = new int[5];
		int len = x.length;
		System.out.println("Simple Array is this long: " + len);
		
		// 2D Array
		System.out.println("2D Array");
//		int[][] arrayA = new int[2][4];
		int[][] arrayA = {{8,9,10,11},{12,13,14,15}};
		System.out.println(arrayA);
		
		// Looping 2D Array
		System.out.println("Looping the 2D Array");
		for (int row=0;row<=1;row++) {
			for (int column=0;column <=3; column++) {
				System.out.print(arrayA[row][column] + "\t");
			}
			System.out.println();
		}
		
		// Irregular Array
		System.out.println("Irregular Array");
		String[][] arrayString = {{"hello", "dolly"}, {"Oh", "hello", "dolly"}, {"oh", "how", "nice", "that", "you"}, {"are", "here"}};
		
		System.out.println("Number of Rows = " + arrayString.length);
		System.out.println("Number of Columns in row 3 = " + arrayString[2].length);
		
		for (int i=0;i< arrayString.length;i++) {
			for (int j=0; j<arrayString[i].length;j++){
				System.out.print(arrayString[i][j] + " ");
			}
			System.out.println();
		}
				
	}

}
