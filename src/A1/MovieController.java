package A1;

import java.text.DecimalFormat;

public class MovieController {
	// Case and Switch Statements
	
	public static void main(String[] args) {
		
//		char ageGroup 		= ' ';
		double cost 		= 00.00;
		double change		= 00.00;
		MovieClass mc 		= new MovieClass();
		MovieGames games	= new MovieGames();
		MovieLoyalty loyal 	= new MovieLoyalty();
		char whichGame		= 'n';
		boolean win 		= false;
		double payment 		= 200;
		
		mc.setDefaultPrice(12.00);
		
		// collect Ticket information
		do {
			// Get Input
			mc.getInput();
			
			// Get Age Group
//			ageGroup = mc.ageGroup;
			mc.getAgeGroup(mc.ageGroup);
			
			// Calculate cost
			cost = mc.calculateCost();
		
			cost = loyal.applyLoyaltyDiscount(cost, mc.numberOfPeople);
			
			// -------------------------------------------
			// Play a game to get Free Tickets
			whichGame = games.whichGameToPlay();
			switch(whichGame)
			{
				case 'm':
					win = playGameMovie(games);
					break;
				case 'h':
					win = playGameHangman(games);
					break;
				case 'n':
					win = false;
					break;
				default:
					win = false;
					break;
			}
			
			
			// Re-Calculate cost
			if (win == true)
			{
//				cost = (win == true) ? 0.00 : mc.calculateCost();
				cost = 0.00;
				System.out.println("***********\nAs you are a WINNER the cost is now: �" + cost + "\n***********");
			}
			
			
			// End of Games
			// -------------------------------------------
			
			System.out.println("\n-----------------------\nThe total cost for " + mc.numberOfPeople + " " + mc.ageGroupVerbose + " tickets is: �" + mc.df.format(cost) + "\n");
			
			// calculate Change
			System.out.println("You have �" + payment + " to purchase these tickets with");
			change = mc.calculateChange(payment, cost);			
		
		} while (mc.numberOfPeople <= 0);
		
		games.end();
		
		
	}
	
	
	public static boolean playGameMovie(MovieGames games)
	{
		String[][] filmData;
		int userChoice 		= 0;
		String userGuess 	= "";
		boolean result;
		boolean response 	= false;
		
		// Play Movie Games
		System.out.println("Setting up Movie Games...");
		filmData = games.setupMovieGame();
		System.out.println("... Movie Games have been initialised.");
		
		userChoice 	= games.getUserChoice();
		userGuess 	= games.getUserGuess(filmData, userChoice);
		response	= games.checkGuess(filmData, userChoice, userGuess);
		
		result = (response == true) ? true : false;
		return result;
	}
	
	
	public static boolean playGameHangman(MovieGames games)
	{
		boolean result = false;
		System.out.println("Hangman does not yet exist - Coming Soon!");
		
		return result;
	}

}
