package A1;

import java.util.*;

public class Lesson1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		double ticketPrice = 0.0;
		int numberPeople = 4;
		int seatNumber = 5;
		char rowNumber = ' ';
		String movieName = "Birdman";
		boolean movieNight = true;
		
//		System.out.print("Hello World"); // Display the string.
//		System.out.print("\nDay\tMovie\tAdult Ticket Price\tChildren\nMonday\tNot a Movie Night\t0\t0"); // Display the string.
//		System.out.println("\nHello World"); // Display the string.
		
		
// ---------------------------------------------------------------------
		
		Scanner myStringScanner  = new Scanner(System.in);
		Scanner myIntScanner 	 = new Scanner(System.in);
		Scanner myBooleanScanner = new Scanner(System.in);
		
		System.out.println("What is the Movies Name? ");
		movieName = myStringScanner.nextLine();
		System.out.println("Tonight is movie night: " + movieName);

		System.out.println("\nHow Many people are going to the movie? ");
		numberPeople = myIntScanner.nextInt();
		System.out.println("The size of the group is: " + numberPeople);
		
		System.out.println("\nThe row number is: " + rowNumber);
		rowNumber = myStringScanner.next().charAt(1);
		System.out.println("The row number is: " + rowNumber);
	
		
	}

}
