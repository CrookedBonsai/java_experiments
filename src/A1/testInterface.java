interface testInterface
{
	// abstract double calculateDiscount(double cost);

	abstract void getInput();

	abstract String getAgeGroup(char ageGroupInput);

	abstract double calculateCost();

	abstract double calculateChange(double payment, double cost);
}