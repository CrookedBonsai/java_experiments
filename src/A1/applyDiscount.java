public class applyDiscount
{
	public static void main (String[] args) 
	{
		double defaultTicketPrice = 0.00;
		if (args.length == 0) 
		{
			System.out.println("Please make sure you input the Default Ticket Price");
		} else {
			System.out.println("Input Param = " + args[0]);
			discount myDiscount = new discount(defaultTicketPrice);
			// movieClass myDiscount = new movieClass(defaultTicketPrice);

			// defaultTicketPrice = Double.parseDouble(args[0]);
			myDiscount.setDefaultPrice(Double.parseDouble(args[0]));
			defaultTicketPrice = myDiscount.getDefaultPrice();
			

			String ageGroupVerbose 	= "";
			double cost 			= 0.00;
			double payment 			= 200;
			double change 			= 0.00;
			double discount 		= 0.00;

			myDiscount.getInput();

			ageGroupVerbose = myDiscount.getAgeGroup(myDiscount.ageGroup);

			cost = myDiscount.calculateCost();

			discount = myDiscount.calculateDiscount(cost);

			change = myDiscount.calculateChange(payment, discount);

			System.out.println("The " + myDiscount.numberOfPeople + " " + ageGroupVerbose + " tickets cost:\t" + cost);
			System.out.println("The cost is:\t" + cost);
			System.out.println("Your discount brings the cost to:\t" + discount);
			System.out.println("You have paid:\t" + payment);
			System.out.println("Your change is:\t" + change);
		}
	}
}