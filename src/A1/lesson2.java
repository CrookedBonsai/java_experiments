package A1;

import java.util.Scanner;
import java.text.DecimalFormat;
//import java.text.NumberFormat;

public class lesson2 {
	// Case and Switch Statements
	
	public static void main(String[] args) {
		int numberOfPeople 		= 0;
		char ageGroup 			= ' ';
		String ageGroupVerbose 	= "";
		double cost 			= 00.00;
		double payment 			= 200;
		double change 			= 0;
		
		Scanner myStringScanner = new Scanner(System.in);
		Scanner myIntScanner 	= new Scanner(System.in);
		DecimalFormat df 		= new DecimalFormat("#0.00");
		
		do {
		
			System.out.println("Welcome\nHow many people are going to the Cinema?");
			numberOfPeople = myIntScanner.nextInt();
			System.out.println("There are " + numberOfPeople + " attending the movies");
			
			System.out.println("Which age group are the film-goers? (Please chose one from below):");
			System.out.println("Adult\t\t=\t'A'\t�12:00\nChild\t\t=\t'C'\t�7:50\nPensioner\t=\t'P'\t�3:00");
			ageGroup = myStringScanner.next().charAt(0);
			
			switch (ageGroup){
				case 'a':
				case 'A': cost = numberOfPeople * 12.00;
						  ageGroupVerbose = "Adult";
						  break;
				case 'c':
				case 'C': cost = numberOfPeople * 7.50;
						  ageGroupVerbose = "Child";
						  break;
				case 'p':
				case 'P': cost = numberOfPeople * 3.00;
						  ageGroupVerbose = "Pensioner";
						  break;
				default:  cost = numberOfPeople * 12.00;
						  ageGroupVerbose = "Default";
			}
			
			System.out.println("The total cost for " + numberOfPeople + " " + ageGroupVerbose + " tickets is: �" + df.format(cost) + "\n");
			
			System.out.println("You have �" + payment + " to purchase these tickets with");
			if (cost <= payment) {
				change = payment - cost;
				System.out.println("Thank you for your payment of �" + df.format(payment) + ", your change is: �" + df.format(change));
			} else {
				change = cost - payment;
				System.out.println("You have insufficient funds, please pay an additional: �" + df.format(change));
			}
		} while (numberOfPeople > 0);
	}

}
