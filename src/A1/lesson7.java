package A1;

public class lesson7 {

	public static void methodPrintMax(String... wish){
		int mtNumberOfArguments = 0;
		if(wish.length == 0) {
			System.out.println("No argument passed");
			return;
		} else {
			mtNumberOfArguments = wish.length;
			for(int i=0; i<= mtNumberOfArguments-1; i++) {
				System.out.println("The values are " + wish[i]);
			}
		}
		
	}
	
	
	public static void methodPrintMax(String wish){

		System.out.println("Overloaded Method Activated: ");
		System.out.println("The value is " + wish);
	}
	
	public static void methodPrintMax(String wish, String wish2){

		System.out.println("Overloaded Two String Method Activated: ");
		System.out.println("The value is " + wish);
		System.out.println("The 2nd value is " + wish2);
	}
	
	
	public static void methodPrintMax(int wish, String wish2){

		System.out.println("Overloaded Int Method Activated: ");
		System.out.println("The value is " + wish);
		System.out.println("The 2nd value is " + wish2);
	}
	
	public static void main(String[] args) {		
		// call method with variable args
		methodPrintMax("Badgers");
		methodPrintMax(500, "Bananas");
		methodPrintMax("Socks", "Bananas");
		methodPrintMax("World Peace", "Health");
		System.out.println(" next lot ");
		methodPrintMax("Shoes", "Mince Pies", "Diamonds", "iPad", "Theatre Tickets");
		
		System.out.println(" ");
		System.out.println(" the End.");
	}

}
