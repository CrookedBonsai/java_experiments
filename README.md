# Java Experiments#

A place to house my experiments with Java.

### What is this repository for? ###

* some basic experiments including simple maths, inheritance and overriding etc.
* Culminating in an application which allows you to book cinema tickets at different prices
* Along with a guess the Actor game for a free ticket.

### Areas of Interest ###

* It is the MovieController.java file which sits at the heart of the Cinema tickets app
* So start here to begin the fun.